/* Adding hemburger menu-active class to the body*/
(function() {
	var $body = document.body
	, $menu_trigger = $body.getElementsByClassName('menu-button')[0];

	if ( typeof $menu_trigger !== 'undefined' ) {
		$menu_trigger.addEventListener('click', function() {
			$body.className = ( $body.className == 'menu-active' )? '' : 'menu-active';
		});
	}
}).call(this);

/* Form label style change depending on input focus, type, value */
$('.contact-form__input').focus(function () {
      $("label[for='" + this.id + "']").addClass('contact-form__label--focused');
});

$('.contact-form__input').focusout(function () {
    if ($(this).val().length == 0) {
        $("label[for='" + this.id + "']").removeClass('contact-form__label--focused');
    }
});

/* Smooth scrolling */
$('a[href*="#"]')
  .click(function(event) {
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
      &&
      location.hostname == this.hostname
    ) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000, function() {
          var $target = $(target);
          $target.focus();
          if ($target.is(":focus")) {
            return false;
          } else {
            $target.attr('tabindex','-1');
            $target.focus();
          };
        });
      }
    }
  });

	/*Disabling and enabling form button*/

	var button = document.querySelector(".contact-form__button");
	var checkbox = document.querySelector(".contact-form__checkbox");

	button.disabled = true;

	checkbox.addEventListener('change', function(event) {
		if(this.checked) {
			button.disabled = false;
	} else {
			button.disabled = true;
	}
	});
