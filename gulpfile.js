var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync');
var autoprefixer = require('gulp-autoprefixer');

gulp.task('serve', ['sass'], function() {

    browserSync.init({
        server: "./dist"
    });
    gulp.watch(["./dev_files/img/*.*", "./dev_files/img/**/*.*"], ['imgcopy']).on('change', browserSync.reload);
    gulp.watch(["./dev_files/stylesheets/*.scss", "./dev_files/stylesheets/**/*.scss"], ['sass']).on('change', browserSync.reload);
    gulp.watch("./dev_files/*.html", ['htmlcopy']).on('change', browserSync.reload);  
    gulp.watch(["./dev_files/js/**/*.js", "./dev_files/js/*.js"], ['jscopy']).on('change', browserSync.reload);  
});

gulp.task('sass', function(){
    gulp.src('./dev_files/stylesheets/main.scss')
        .pipe(sass())
        .pipe(autoprefixer({
            browsers: ['last 2 versions']
        }))
        .pipe(gulp.dest('./dist/style'));
});

gulp.task('htmlcopy', function(){
    gulp.src('./dev_files/*.html')
        .pipe(gulp.dest('./dist'));
});

gulp.task('imgcopy', function(){
    gulp.src('./dev_files/img/**/*.*')
        .pipe(gulp.dest('./dist/img'));
});

gulp.task('jscopy', function() {
    gulp.src('./dev_files/js/**/*.js')
        .pipe(gulp.dest('./dist/js'));
})

gulp.task('default', ['sass', 'imgcopy', 'htmlcopy', 'jscopy', 'serve']);